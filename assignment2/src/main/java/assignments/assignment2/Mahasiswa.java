package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // Membuat constructor untuk Mahasiswa.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // Mengembalikan KomponenPenilaian yang bernama namaKomponen.
        // foreach untuk mengakses element di komponenPenilaian
        for (KomponenPenilaian element : this.komponenPenilaian) {
            if (element.getNama().equals(namaKomponen)) {
                return element;
            } else {
                continue;
            }
        }
        return null; // Mengembalikan null jika tidak ada element
    }

    public String getNpm() {
        // Mengembalikan NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     *
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
                nilai >= 80 ? "A-" :
                        nilai >= 75 ? "B+" :
                                nilai >= 70 ? "B" :
                                        nilai >= 65 ? "B-" :
                                                nilai >= 60 ? "C+" :
                                                        nilai >= 55 ? "C" :
                                                                nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     *
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        // Mengembalikan rekapan sesuai dengan permintaan soal.
        String rekapTemp = "";
        double totalNilai = 0.0;

        // foreach untuk mengakses element di komponenPenilaian
        for (KomponenPenilaian element : komponenPenilaian) {
            rekapTemp += String.format("%s\n",element.toString());
            totalNilai += element.getNilai(); // totalNilai sebagai Nilai akhir
        }

        // Melakukan konkatenasi sesuai dengan permintaan soal
        rekapTemp += String.format("Nilai akhir: %.2f\n", totalNilai);
        rekapTemp += String.format("Huruf: %s\n", getHuruf(totalNilai));
        rekapTemp += getKelulusan(totalNilai);

        return rekapTemp;
    }

    public String toString() {
        // Mengembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return String.format("%s - %s", this.npm, this.nama);
    }

    public String getDetail() {
        // Mengembalikan detail dari Mahasiswa sesuai permintaan soal.
        String detail = "";
        double nilaiAkhir = 0;

        // foreach untuk mengakses element di komponenPenilaian
        for (KomponenPenilaian element : komponenPenilaian) {
            detail += element.getDetail();
            nilaiAkhir += element.getNilai();
        }

        // Melakukan konkatenasi sesuai dengan permintaan soal
        detail += String.format("Nilai akhir: %.2f \n", nilaiAkhir);
        detail += String.format("%s \n", getHuruf(nilaiAkhir));
        detail += getKelulusan(nilaiAkhir);

        return detail;
    }

    @Override
    public int compareTo(Mahasiswa other) {
        // Mendefinisikan cara membandingkan seorang mahasiswa dengan mahasiswa lainnya.
        // Menggunakan method compareTo
        String npm1 = this.npm;
        String npm2 = other.getNpm();
        return npm1.compareTo(npm2);
    }
}
