package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // Membuat constructor untuk KomponenPenilaian.
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     *
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     *
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // Memasukkan butir ke butirPenilaian pada index ke-idx.
        this.butirPenilaian[idx] = butir;
    }

    public String getNama() {
        // Mengembalikan nama KomponenPenilaian.
        return this.nama;
    }

    public double getRerata() {
        // Mengembalikan rata-rata butirPenilaian.
        double rerata = 0;
        int temp = 0;

        // foreach untuk mengakses element di butirPenilaian
        for (ButirPenilaian element : butirPenilaian) {
            if (element != null) {
                rerata += element.getNilai();
                temp++; // temp sebagai banyak nilai yang bukan null
            }
        }

        // Membuat kondisi apabila nilai belum diinput, maka mengembalikan 0
        if (temp == 0) {
            return 0.0;
        }

        return rerata / temp;
    }

    public double getNilai() {
        // Mengembalikan rerata yang sudah dikalikan dengan bobot.
        return this.getRerata() * (double) this.bobot / 100;
    }

    public String getDetail() {
        // Mengembalikan detail KomponenPenilaian sesuai permintaan soal.
        String detail = "";
        int banyak = 1;

        // Judul
        detail += String.format("~~~ %s (%d) ~~~\n", this.nama, this.bobot);

        // Membuat kondisi jika panjang array lebih dari satu
        if (butirPenilaian.length > 1) {
            // foreach utk mengakses element di butirPenilaian
            for (ButirPenilaian lab : butirPenilaian) {
                if (lab != null) {
                    detail += String.format("%s %d: %s \n", this.nama, banyak++, lab.toString());
                } //else if (lab == null){
                //     detail += String.fotmat("%s %d: %f\n", this.nama, banyak++, 0.00);
                // }
            }
            detail += String.format("Rerata: %.2f \n", getRerata());
        }
        // Membuat kondisi jika panjang array tepat 1
        else {
            if (butirPenilaian[0] != null) {
                // foreach utk mengakses element di butirPenilaian
                for (ButirPenilaian tp : butirPenilaian) {
                    detail += String.format("%s %d: %s \n", this.nama, banyak++, tp.toString());
                }
            }
        }
        detail += String.format("Kontribusi nilai akhir: %.2f \n", this.getNilai());
        return detail;
    }

    @Override
    public String toString() {
        // Mengembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        return String.format("Rerata %s: %.2f", this.nama, getRerata());
    }

}
