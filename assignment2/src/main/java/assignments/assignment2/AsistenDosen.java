package assignments.assignment2;

import java.util.Collections;

import java.util.ArrayList;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        // Membuat constructor untuk AsistenDosen.
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        // Mengembalikan kode AsistenDosen.
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // Menambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa); // Sorting mahasiswa berdasarkan urutan menggunakan Collections
    }

    public Mahasiswa getMahasiswa(String npm) {
        // Mengembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // foreach untuk mengakses element di mahasiswa
        for (Mahasiswa element : mahasiswa) {
            if (element.getNpm().equals(npm)) {
                return element;
            }
        }
        return null; // Mengembalikan null jika npm element != npm
    }

    public String rekap() {
        String rekapRes = "";
        // foreach untuk mengakses element di mahasiswa
        for (Mahasiswa element : mahasiswa) {
            // Melakukan konkatenasi sesusai dengan permintaan soal
            rekapRes += String.format("%s", element.toString());
            rekapRes += String.format("%s", element.rekap());
        }
        return rekapRes;
    }

    public String toString() {
        // Mengembalikan representasi String berupa kode dan nama sesuai dg permintaan soal
        return String.format("%s - %s", getKode(), this.nama);
    }
}
