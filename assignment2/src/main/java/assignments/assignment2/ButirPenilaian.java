package assignments.assignment2;

// main class ButirPenilaian
public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        // membuat constructor untuk ButirPenilaian.
        if (nilai < 0) {
            this.nilai = 0;
        } else {
            this.nilai = nilai;
        }
        this.terlambat = terlambat;
    }

    /**
     * Method getNilai
     *
     * @return nilai yg sudah dipenalti
     */
    public double getNilai() {
        // mengembalikan nilai yang sudah disesuaikan dengan keterlambatan.
        if (terlambat == true) {
            return this.nilai - ((this.nilai * PENALTI_KETERLAMBATAN) / 100);
        } else {
            return this.nilai;
        }
    }

    /**
     * Method toString
     *
     * @return String berisi nilai yg sudah ditandai T jika terlambat
     */
    @Override
    public String toString() {
        // mengembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
        if (terlambat == true) {
            return String.format("%.2f (T)", getNilai());
        } else {
            return String.format("%.2f", getNilai());
        }
    }
}
