package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{
    
    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama,String tipe){
        // Membuat constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
        this.statusCovid = new Negatif();
        this.rantaiPenular = new ArrayList<>();
    }

    public String getNama(){
        // Mengembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe(){
        // Mengembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid(){
        // Mengembalikan nilai dari atribut statusCovid
        return statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        // Mengembalikan nilai dari atribut aktifKasusDisebabkan
        return this.aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan(){
        // Mengembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular(){
        // Mengembalikan nilai dari atribut rantaiPenular
        return this.rantaiPenular;
    }

    public void ubahStatus(String status){
        // Mengimplementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if (status.equalsIgnoreCase("Negatif")){
            statusCovid = new Negatif();
        }
        else if (status.equalsIgnoreCase("Positif")){
            statusCovid = new Positif();
        }
    }

    public void incrementTotalKasusDisebabkan(){
        // Method untuk menambahkan jumlah total kasus
        totalKasusDisebabkan++;
    }

    public void incrementAktifKasusDisebabkan(){
        // Method untuk menambahkan jumlah aktif kasus
        aktifKasusDisebabkan++;
    }

    public void decrementAktifKasusDisebabkan(){
        // Method untuk mengurangi jumlah aktif kasus
        aktifKasusDisebabkan--;
    }

    public void incrementRantaiPenular(Carrier carrier){
        // Membuat copy-an list rantaiPenular
        List<Carrier> copy = new ArrayList<>();

        // Memasukan objek ke list rantaiPenular
        this.getRantaiPenular().addAll(carrier.getRantaiPenular());
        this.getRantaiPenular().add(carrier);

        // foreach untuk menghilangkan object double
        for (Carrier e : this.rantaiPenular){
            if (!copy.contains(e)){
                copy.add(e);
            }
        }

        // foreach untuk menambahkan total dan aktif kasus dari objek
        for (Carrier c : copy){
            c.incrementTotalKasusDisebabkan();
            c.incrementAktifKasusDisebabkan();
        }
    }

    public void interaksi(Carrier lain){
        // Objek ini berinteraksi dengan objek lain
        this.statusCovid.tularkan(this, lain);
        lain.statusCovid.tularkan(lain, this);
    }

    public abstract String toString();

}
