package assignments.assignment3;


public abstract class Benda extends Carrier{
  
    protected int persentaseMenular;

    public Benda(String name){
        // Membuat constructor untuk Benda dengan akses superclass-nya
        super(name, "Benda");
        this.persentaseMenular = 0;
    }

    public abstract void tambahPersentase();

    public int getPersentaseMenular(){
        // Mengembalikan nilai dari atribut persentaseMenular
        return this.persentaseMenular;
    }
    
    public void setPersentaseMenular(int persentase) {
        // Menggunakan sebagai setter untuk atribut persentase menular
        this.persentaseMenular = persentase;
    }
}