package assignments.assignment3;

public class Jurnalis extends Manusia{
  	
    public Jurnalis(String name){
        // Membuat constructor untuk Jurnalis dengan akses superclass-nya
        super(name);
    }

    public String toString(){
        return String.format("JURNALIS %s", getNama());
    }
}