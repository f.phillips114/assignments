package assignments.assignment3;

public class PekerjaJasa extends Manusia{
  	
    public PekerjaJasa(String nama){
    	// Membuat constructor untuk PekerjaJasa dengan akses superclass-nya
        super(nama);
    }

    public String toString(){
        return String.format("PEKERJA JASA %s", getNama());
    }
}