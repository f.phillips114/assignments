package assignments.assignment3;

public class Positif implements Status{
  
    public String getStatus(){
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        // Mengimplementasikan apabila object Penular melakukan interaksi dengan object tertular

        // Ketika manusia berinteraksi dengan manusia
        if(penular.getTipe().equalsIgnoreCase("Manusia") && tertular.getTipe().equalsIgnoreCase("Manusia")){
            if(tertular.getStatusCovid().equalsIgnoreCase("Negatif")){
                tertular.ubahStatus("Positif");
                tertular.incrementRantaiPenular(penular);
            }
        }

        // Ketika manusia berinteraksi dengan benda
        else if(penular.getTipe().equalsIgnoreCase("Manusia") && tertular.getTipe().equalsIgnoreCase("Benda")){
            Benda benda = (Benda) tertular;
            if (benda instanceof Pintu){
                benda.setPersentaseMenular(benda.getPersentaseMenular() + 30);
            }
            else if (benda instanceof AngkutanUmum){
                benda.setPersentaseMenular(benda.getPersentaseMenular() + 35);
            }
            else if (benda instanceof PeganganTangga || benda instanceof TombolLift){
                benda.setPersentaseMenular(benda.getPersentaseMenular() + 20);
            }

            if(benda.getPersentaseMenular() >= 100){
                tertular.ubahStatus("Positif");
                tertular.incrementRantaiPenular(penular);
            }
        }

        // Ketika benda berinteraksi dengan manusia
        else if(penular.getTipe().equalsIgnoreCase("Benda") && tertular.getTipe().equalsIgnoreCase("Manusia")){
            if(penular.getStatusCovid().equalsIgnoreCase("Positif") && tertular.getStatusCovid().equalsIgnoreCase("Negatif")){
                tertular.ubahStatus("Positif");
                tertular.incrementRantaiPenular(penular);
            }
        }
    }
}