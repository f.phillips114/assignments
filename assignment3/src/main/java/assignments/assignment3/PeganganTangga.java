package assignments.assignment3;

public class PeganganTangga extends Benda{
    // Mengimplementasikan abstract method yang terdapat pada class Benda
  	
    public PeganganTangga(String name){
        // Membuat constructor untuk PeganganTangga dengan akses superclass-nya
        super(name);
    }

    public void tambahPersentase(){
        // Override abstract method dari class Benda 
        int persentase = getPersentaseMenular() + 20;
        setPersentaseMenular(persentase);
    }

    public String toString() {
        return String.format("PEGANGAN TANGGA %s", getNama());
    }  
}