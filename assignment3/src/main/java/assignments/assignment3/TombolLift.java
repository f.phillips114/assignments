package assignments.assignment3;

public class TombolLift extends Benda{
      // Mengimplementasikan abstract method yang terdapat pada class Benda
      
    public TombolLift(String name){
        // Membuat constructor untuk TombolLift dengan akses superclass-nya
        super(name);
    }

    public void tambahPersentase(){
        // Override abstract method dari class Benda 
        int persentase = getPersentaseMenular() + 20;
        setPersentaseMenular(persentase);
    }

    public String toString(){
        return String.format("TOMBOL LIFT %s", getNama());
    }
}