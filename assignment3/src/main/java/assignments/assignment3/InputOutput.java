package assignments.assignment3;

import java.io.*;
import java.util.*;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile){
        // Membuat constructor untuk InputOutput.
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
    }

    public void setBufferedReader(String inputType){
        // Membuat BufferedReader bergantung inputType (I/O text atau input terminal) 
        if (inputType.equalsIgnoreCase("terminal")) {
            this.br = new BufferedReader(new InputStreamReader(System.in));
        } 
        else if (inputType.equalsIgnoreCase("text")) {
            try {
                this.br = new BufferedReader(new FileReader(this.inputFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void setPrintWriter(String outputType){
        // Membuat PrintWriter bergantung inputType (I/O text atau output terminal) 
        if (outputType.equalsIgnoreCase("terminal")) {
            this.pw = new PrintWriter(System.out);
        } 
        else if (outputType.equalsIgnoreCase("text")) {
            try {
                this.pw = new PrintWriter(new File(this.outputFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void run() throws IOException, BelumTertularException{
        // Program utama untuk InputOutput
        // Menggunakan method CreateObject pada class World untuk membuat object Carrier baru
        // Menggunakan method getCarrier pada class World untuk mengambil object Carrier

        this.world = new World();      // Membuat objek baru pada class World
        Carrier carrierTemp;
        PetugasMedis medis;
        CleaningService clean;
        String query = this.br.readLine();
        
        // looping sesuai dengan inputan user dan berhenti ketika user input "EXIT"
        while (!query.equalsIgnoreCase("EXIT")) {
            String[] querySplit = query.split(" ");
            if (querySplit[0].equalsIgnoreCase("rantai")){
                try{
                    carrierTemp = this.world.getCarrier(querySplit[1]);
                    if (carrierTemp.getStatusCovid().equalsIgnoreCase("Negatif")) {
                        String result = String.format("%s berstatus negatif", carrierTemp);
                        throw new BelumTertularException(result); 
                    } 
                    List<Carrier> rantai = carrierTemp.getRantaiPenular();
                    String result = String.format("Rantai penyebaran %s: ", carrierTemp);
                    pw.print(result);
                    for(Carrier carrier : rantai){
                        pw.print(carrier + " -> " );
                    }
                    pw.print(carrierTemp);
                    pw.println("");
                } catch (BelumTertularException e){
                    pw.println(e.toString());
                }
            }
            else if (querySplit[0].equalsIgnoreCase("sembuhkan")){
                medis = (PetugasMedis) this.world.getCarrier(querySplit[1]);
                medis.obati((Manusia) this.world.getCarrier(querySplit[2]));
            } 
            else if (querySplit[0].equalsIgnoreCase("bersihkan")){
                clean = (CleaningService) this.world.getCarrier(querySplit[1]);
                clean.bersihkan((Benda) this.world.getCarrier(querySplit[2]));
            } 
            else if (querySplit[0].equalsIgnoreCase("add")){
                this.world.createObject(querySplit[1], querySplit[2]);
            }
            else if (querySplit[0].equalsIgnoreCase("interaksi")){
                this.world.getCarrier(querySplit[1]).interaksi(this.world.getCarrier(querySplit[2]));
            }
            else if (querySplit[0].equalsIgnoreCase("positifkan")){
                this.world.getCarrier(querySplit[1]).ubahStatus("Positif");
            }
            else if (querySplit[0].equalsIgnoreCase("total_kasus_dari_objek")){
                carrierTemp = this.world.getCarrier(querySplit[1]);
                String result = String.format("%s telah menyebarkan virus COVID ke %d objek", carrierTemp, carrierTemp.getTotalKasusDisebabkan());
                pw.println(result);
            } 
            else if (querySplit[0].equalsIgnoreCase("aktif_kasus_dari_objek")){
                carrierTemp = this.world.getCarrier(querySplit[1]);
                String result = String.format("%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak %d objek", carrierTemp, carrierTemp.getAktifKasusDisebabkan());
                pw.println(result);
            } 
            else if (querySplit[0].equalsIgnoreCase("total_sembuh_manusia")){
                String result = String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus", Manusia.getJumlahSembuh());
                pw.println(result);
            } 
            else if (querySplit[0].equalsIgnoreCase("total_sembuh_petugas_medis")){
                medis = (PetugasMedis) this.world.getCarrier(querySplit[1]);
                String result = String.format("%s menyembuhkan %d manusia", medis, medis.getJumlahDisembuhkan());
                pw.println(result);
            } 
            else if (querySplit[0].equalsIgnoreCase("total_bersih_cleaning_service")){
                clean = (CleaningService) this.world.getCarrier(querySplit[1]);
                String result = String.format("%s membersihkan %d benda", clean, clean.getJumlahDibersihkan());
                pw.println(result);
            } 
            else if (querySplit[0].equalsIgnoreCase("EXIT")){
                break;
            } 
            else {
                pw.println("Perintah tidak dapat dijalankan!");
            }
            query = this.br.readLine();
        }
        pw.flush();
    }
}