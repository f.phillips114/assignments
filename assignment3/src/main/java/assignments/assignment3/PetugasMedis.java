package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        // Membuat constructor untuk PetugasMedis dengan akses superclass-nya
        super(nama);
    }

    public void obati(Manusia manusia) {
        // Mengimplementasikan apabila objek PetugasMedis ini menyembuhkan manusia

        if (manusia.getStatusCovid().equalsIgnoreCase("Positif")){
            manusia.ubahStatus("Negatif");
            jumlahDisembuhkan++;
            tambahSembuh();
        }

        // foreach untuk mengurangi jumlah aktifKasus pada carrier
        for (Carrier carrier : manusia.getRantaiPenular()){
            carrier.decrementAktifKasusDisebabkan();
        }

        manusia.getRantaiPenular().clear();
    }

    public int getJumlahDisembuhkan(){
        // Mengembalikan nilai dari atribut jumlahDisembuhkan
        return jumlahDisembuhkan;
    }

    public String toString(){
        return String.format("PETUGAS MEDIS %s", getNama());
    }
}