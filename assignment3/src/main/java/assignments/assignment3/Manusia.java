package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh = 0;
    
    public Manusia(String nama){
        // Membuat constructor untuk Manusia dengan akses superclass-nya
        super(nama, "Manusia");

    }
    
    public void tambahSembuh(){
        // Menambahkan nilai pada atribut jumlahSembuh
        jumlahSembuh++;
    }

    public static int getJumlahSembuh() {
        // Mengembalikan nilai untuk atribut jumlahSembuh
        return jumlahSembuh;
    }
    
}