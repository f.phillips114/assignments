package assignments.assignment3;

public class Pintu extends Benda{
    // Mengimplementasikan abstract method yang terdapat pada class Benda  
    
    public Pintu(String name){
        // Membuat constructor Pintu dengan akses superclass-nya
        super(name);
    }

    public void tambahPersentase(){
        // Override abstract method dari class Benda 
        int persentase = getPersentaseMenular() + 30;
        setPersentaseMenular(persentase);
    }

    public String toString(){
        return String.format("PINTU %s", getNama());
    }
}