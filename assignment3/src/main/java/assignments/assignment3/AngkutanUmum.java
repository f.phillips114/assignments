package assignments.assignment3;

public class AngkutanUmum extends Benda{
    // Mengimplementasikan abstract method yang terdapat pada class Benda
      
    public AngkutanUmum(String name){
        // Membuat constructor untuk AngkutanUmum dengan akses superclass-nya
        super(name);
    }

    public void tambahPersentase(){
        // Override abstract method dari class Benda 
        int persentase = getPersentaseMenular() + 35;
        setPersentaseMenular(persentase);
    }

    public String toString(){
        return String.format("ANGKUTAN UMUM %s", getNama());
    }
}