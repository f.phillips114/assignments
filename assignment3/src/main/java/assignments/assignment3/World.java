package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        // Membuat constructor untuk class World
        listCarrier = new ArrayList<Carrier>();
    }

    public Carrier createObject(String tipe, String nama){
        // Mengimplementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        Carrier carrier = null;

        if (tipe.equals("PETUGAS_MEDIS")){
            carrier = new PetugasMedis(nama);
            listCarrier.add(carrier);
        }
        else if (tipe.equals("PEKERJA_JASA")){
            carrier = new PekerjaJasa(nama);
            listCarrier.add(carrier);
        }
        else if (tipe.equals("JURNALIS")){
            carrier = new Jurnalis(nama);
            listCarrier.add(carrier);
        }
        else if (tipe.equals("OJOL")){
            carrier = new Ojol(nama);
            listCarrier.add(carrier);
        }
        else if (tipe.equals("CLEANING_SERVICE")){
            carrier = new CleaningService(nama);
            listCarrier.add(carrier);
        }
        else if (tipe.equals("PEGANGAN_TANGGA")){
            carrier = new PeganganTangga(nama);
            listCarrier.add(carrier);
        }
        else if (tipe.equals("PINTU")){
            carrier = new Pintu(nama);
            listCarrier.add(carrier);
        }
        else if (tipe.equals("TOMBOL_LIFT")){
            carrier = new TombolLift(nama);
            listCarrier.add(carrier);
        }
        else if (tipe.equals("ANGKUTAN_UMUM")){
            carrier = new AngkutanUmum(nama);
            listCarrier.add(carrier);
        }
        return carrier;
    }

    public Carrier getCarrier(String nama){
        // Mengimplementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        for (Carrier carrier : listCarrier){
            if (nama.equals(carrier.getNama())){
                return carrier;
            }
        } return null;
    }
}
