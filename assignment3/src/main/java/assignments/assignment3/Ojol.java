package assignments.assignment3;

public class Ojol extends Manusia{
  	
    public Ojol(String name){
		// Membuat constructor untuk Ojol dengan akses superclass-nya
        super(name);
    }
      
    public String toString(){
        return String.format("OJOL %s", getNama());
    }
}