package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){
        // Membuat constructor untuk CleaningService dengan akses superclass-nya
        super(nama);
    }

    public void bersihkan(Benda benda){
        // Mengimplementasikan apabila objek CleaningService ini membersihkan benda
        if (benda.persentaseMenular >= 100  || benda.getStatusCovid().equalsIgnoreCase("Positif")) {
            benda.persentaseMenular = 0;
            benda.ubahStatus("Negatif");
            jumlahDibersihkan++;
        }
        else if (benda.persentaseMenular >= 0 && benda.persentaseMenular < 100) {
            benda.persentaseMenular = 0;
            jumlahDibersihkan++;
        } 

        // foreach untuk mengurangi aktifKasus pada carrier jika persentase benda 0
        for (Carrier carrier : benda.getRantaiPenular()){
            carrier.decrementAktifKasusDisebabkan();
        }
    }

    public int getJumlahDibersihkan(){
        // Mengembalikan nilai dari atribut jumlahDibersihkan
        return jumlahDibersihkan;
    }

    public String toString(){
        return String.format("CLEANING SERVICE %s", getNama());
    }

}