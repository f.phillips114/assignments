package assignments.assignment1;

import java.util.ArrayList;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     * Main method for encode.
     * 
     * @param data is the input data
     * @return the encoded Hamming Code.
     */
    public static String encode(String data) {
        //assign nilai r ke var r
        int r = searchREncode(data.length());
        //System.out.println("R is " + r);
        ArrayList<String> arrayLst = new ArrayList<String>();

        //loop untuk buat arraylst of string
        for (int i = 0; i < data.length(); i++) {
            arrayLst.add((data.substring(i, i + 1)));
        }
        //System.out.println(arrayLst);
        //System.out.println("##");

        //loop untuk sisipin nilai default 0 di index 0,1,2,4,8
        for (int i = 0; i < r; i++) {
            arrayLst.add((int) Math.pow(2, i) - 1, "0");
        }
        //System.out.println(arrayLst);
        //System.out.println("###");

        //loop untuk print hasil final berupa arraylist
        for (int k = 0; k < r; k++) {
            arrayLst = parityEncode(arrayLst, ((int) Math.pow(2, k)));
        }
        //System.out.println(arrayLst);
        //System.out.println("####");

        //loop foreach untuk print result berupa string
        String result = "";
        for (String string : arrayLst) {
            result += string;
        }
        System.out.println("Result encode is: " + result);
        return result;
    }

    /**
     * Method for search how many bit redundant in encode.
     * 
     * @param i is the number of actual data bits
     * @return the number of bit redundant in encode.
     */
    public static int searchREncode(int i) {
        //find jumlah r
        int x = 1;
        int temp = 0;
        boolean findR = false;
        do {
            temp = x;
            findR = (int) Math.pow(2, x) < (i + x + 1);
            x++;
        } while (findR);
        return temp;
    }

    /**
     * Method for looping bit parity in encode.
     * 
     * @param a is an ArrayList that'll be assigned with the correct code
     * @param r is the number of bit redundant
     * @return the corrected encode code .
     */
    public static ArrayList parityEncode(ArrayList<String> a, int r) {
        int temp = 0;
        for (int i = r - 1; i < a.size(); i += r * 2) {  //loop untuk skip nilai
            for (int j = i; j < i + r; j++) {         //loop untuk jumlahin nilai parity
                if (a.size() > j) {
                    temp += Integer.parseInt(a.get(j));   //masukin hasil jml ke var temp
                    //System.out.println("Temp in nested loop is: " + temp);
                }
            }
        }
        //condition untuk check kl jumlah parity genap dia akan return 0 kl ganjil 1
        if (temp % 2 != 0) {
            //System.out.println("Temp in if is: " + temp);
            a.set(r - 1, "1");
            //System.out.println("Code masuk temp % 2 != 0");
        } else {
            //System.out.println("Temp in else is: " + temp);
            a.set(r - 1, "0");
            //System.out.println("Code masuk else");
        }
        return a;
    }

    /**
     * Main method for decode.
     * 
     * @param data is the input data
     * @return the decoded Hamming Code.
     */
    public static String decode(String data) {
        //assign nilai r ke var R
        int r = searchRDecode(data.length());
        //System.out.println("R is " + r);
        ArrayList<String> arrayLstDecode = new ArrayList<String>();
        //return Integer.toString(R);

        //loop untuk buat arrayLstDecode of string
        for (int i = 0; i < data.length(); i++) {
            arrayLstDecode.add((data.substring(i, i + 1)));
        }
        // System.out.println("Array list decode: " + arrayLstDecode);
        // System.out.println("##");

        //loop untuk jumlahin value parity yang error
        int error = 0;
        for (int k = 0; k < r; k++) {
            //System.out.println("R in for k is: " + r);
            error += parityDecode(arrayLstDecode, ((int) Math.pow(2, k)));
            //System.out.println("Error is: " + error);
        }

        // System.out.println("Array list decode: " + arrayLstDecode);
        // System.err.println("errror = " + error);

        //buat condition kl hammingcode nya error  
        //index penjumlahan yg error tsb diganti jd kebalikannya
        if (error != 0) {
            if (arrayLstDecode.get(error - 1).equals("1")) {
                //System.out.println("masuk if");
                arrayLstDecode.set(error - 1, "0");
            } else {
                //System.out.println("masuk else");
                arrayLstDecode.set(error - 1, "1");
            }
        }

        //loop remove parity
        for (int k = r - 1; k >= 0; k--) {
            arrayLstDecode.remove((int) Math.pow(2, k) - 1);
        }

        // System.out.println("Array list decode: " + arrayLstDecode);
        // System.out.println("####");

        //loop foreach untuk print result berupa string
        String result = "";
        for (String string : arrayLstDecode) {
            result += string;
        }
        System.out.println("Result decode is: " + result);
        return result;
    }

    /**
     * Method for search how many bit redundant in decode.
     * 
     * @param i is the number of actual data bits
     * @return the number of bit redundant in decode.
     */
    public static int searchRDecode(int i) {
        //find r di decode
        int x = 1;
        int temp = 0;
        boolean findR = false;
        do {
            temp = x;
            findR = (int) Math.pow(2, x) < (i);
            x++;
        } while (findR);
        return temp;
    }

    /**
     * Method for looping bit parity in decode.
     * 
     * @param b is an ArrayList that'll be assigned with the correct code
     * @param r is the number of bit redundant
     * @return the corrected decode code.
     */
    public static int parityDecode(ArrayList<String> b, int r) {
        int temp = 0;
        int res = 0;
        for (int i = r - 1; i < b.size(); i += r * 2) { //loop untuk skip nilai
            for (int j = i; j < r + i; j++) {        //loop untuk jumlahin nilai parity
                if (b.size() > j) {
                    temp += Integer.parseInt(b.get(j));
                    //System.out.println("Temp in nested loop is: " + temp);
                }
            }
        }
        //condition untuk cek kl dia ganjil maka simpan dan kembaliin nilai paritynya ke var res
        //System.out.println("temp" + temp);
        if (temp % 2 != 0) {
            //System.out.println("R in if is: " + r);
            res += r;
        } else {
            res = 0;
        }
        return res;
    }

    /**
     * Main program for Hamming Code.
     *
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
